## Transfer Game Ownership

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct user address.
const toBeOwner = '0xbA418a52A50c7169dbf7296D64B45a82DFa093Ce';

// Object containing the data needed to transfer game ownership.
const transferData = {
    gameAddr,
    from: walletAddress,
    to: toBeOwner,
    timestamp: Date.now()
};

// Sign the data with your private key.
const transferDataJSON = stardust.createPostJSON.game.transferJSON(transferData, privateKey);

// POST to the API endpoint '/v1/games/transfer'.
// Send the transferDataJSON object in the request body.
// With the game address in the parameters.
const response = () => axios.post('/v1/games/transfer', transferDataJSON, {params: {gameAddr}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/games/transfer`

This endpoint transfers game ownership to the receiving player address.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/games/transfer`

### Arguments

Attribute       | Type      | Description                                       | Required  | Max. length   |
--------------- | --------- | ------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                     | Yes       |       -       |
from            | string    | Address of the current game owner.                | Yes       |       -       |
to              | string    | Address of the new game owner.                    | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization. | Yes       |       -       |