# Games

## Games

- What is the Games API used for?
- Prerequisites to use the Games API

## Create Game

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Object containing the data needed to create a game.
const deployData = {
    name: 'Game Name',
    desc: 'Game Description',
    image: 'Game Image',
    timestamp: Date.now()
};

// Sign the data with your private key.
const deployDataJSON = stardust.createPostJSON.game.deploy(deployData, privateKey);

// POST to the API endpoint '/v1/games'.
// Send the deployDataJSON object in the request body.
const response = () => axios.post('/v1/games', deployDataJSON).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/games`

This endpoint creates a new game.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/games`

### Arguments

Attribute       | Type      | Description                                       | Required  | Max. length   |
--------------- | --------- | ------------------------------------------------- | --------- | ------------- |
name            | string    | Name of the game.                                 | Yes       |       -       |
desc            | string    | Description of the game.                          | Yes       |       -       |
image           | string    | Image of the game.                                | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization. | Yes       |       -       |