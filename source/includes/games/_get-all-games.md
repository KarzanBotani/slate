## Get All Games

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// GET all games from the API endpoint '/v1/games'.
const response = () => axios.get('/v1/games').then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/games`

This endpoint retrieves all games.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/games`