# Trade

## Trade

- What is the Trade API used for?
- Prerequisites to use the Trade API.

## Offer Private Trade

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Object containing the data needed to offer a private trade.
const offerData = {
    gameAddr,
    taker: '0xbA418a52A50c7169dbf7296D64B45a82DFa093Ce', // Exchange this for the correct user address.
    offeredId: 0, // Exchange this for the correct asset id.
    offeredAmount: 1,
    wantedId: 1, // Exchange this for the correct asset id.
    wantedAmount: 1,
    timestamp: Date.now()
}

// Sign the data with your private key.
const offerDataJSON = stardust.createPostJSON.trade.offerPrivate(tradeData, privateKey);

// POST to the API endpoint '/v1/trades/offer-private'.
// Send the offerDataJSON object in the request body.
// With the game address in the parameters.
const response = () => axios.post('/v1/trades/offer-private', offerDataJSON, {params: {gameAddr}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/trades/offer-private`

This endpoint offers a private trade.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/trades/offer-private`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
taker           | string    | Address of the person to trade with.                  | Yes       |       -       |
borrower        | string    | Address of the borrower.                              | Yes       |       -       |
offeredId       | number    | Id the asset to be traded.                            | Yes       |       -       |
offeredAmount   | number    | Amount to be offered.                                 | Yes       |       -       |
wantedId        | number    | Id the asset to be traded for.                        | Yes       |       -       |
wantedAmount    | number    | Amount wanted.                                        | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |