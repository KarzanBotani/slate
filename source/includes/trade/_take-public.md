## Take Public Trade

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct trade index.
const index = 0;

// Object containing the data needed to accept a public trade.
const takeData = {
    gameAddr,
    index,
    timestamp: Date.now()
}

// Sign the data with your private key.
const takeDataJSON = stardust.createPostJSON.trade.takePublic(tradeData, privateKey);

// POST to the API endpoint '/v1/trades/take-public'.
// Send the takeDataJSON object in the request body.
// With the game address and index in the parameters.
const response = () => axios.post('/v1/trades/take-public', offerDataJSON, {params: {gameAddr, index}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/trades/take-public`

This endpoint accept a public trade.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/trades/take-public`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
index           | number    | Index of the trade.                                   | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |