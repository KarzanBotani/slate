## Get All User Trades

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// GET all trades by a user from the API endpoint '/v1/trades/game'.
// With the game address and user address in the parameters.
const response = () => axios.get('/v1/trades/game/id', {params: {gameAddr, userAddr: walletAddress}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/trades/game/id`

This endpoint retrieves all trades by a user from a specific game.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/trades/game/id`

### Arguments

Attribute    | Type      | Description                          | Required  | Max. length   |
------------ | --------- | ------------------------------------ | --------- |-------------- |
gameAddr     | string    | Address of the game contract.        | Yes       |       -       |
userAddr     | string    | Address of the user.                 | Yes       |       -       |