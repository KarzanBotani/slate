## Get User Trade Count

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// GET user trade count across all games from the API endpoint '/v1/trades/user-count'.
// With the user address in the parameters.
const response = () => axios.get('/v1/trades/user-count', {params: {userAddr: walletAddress}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/trades/user-count`

This endpoint retrieves a user's trade count across all games.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/trades/user-count`

### Arguments

Attribute    | Type      | Description                          | Required  | Max. length   |
------------ | --------- | ------------------------------------ | --------- |-------------- |
userAddr     | string    | Address of the user.                 | Yes       |       -       |