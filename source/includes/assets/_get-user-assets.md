## Get User Assets

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// GET all assets owned by a specific user in a specific game from the API endpoint '/v1/assetsOf'.
// With the game address and user address in the parameters.
const response = () => axios.get('/v1/assetsOf', {params: {gameAddr, userAddr: walletAddress}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/assetsOf`

This endpoint retrieves all assets owned by a specific user in a specific game.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/assetsOf`

### Arguments

Attribute       | Type      | Description                           | Required  | Max. length   |
--------------- | --------- | ------------------------------------- | --------- |-------------- |
gameAddr        | string    | Address of the game contract.         | Yes       |       -       |
userAddr        | string    | Address of the user to query.         | Yes       |       -       |