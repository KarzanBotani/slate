## Get Specific Asset

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const assetId = 0;

// GET a specific asset in a specific game from the API endpoint '/v1/assets'.
// With the game address and asset id in the parameters.
const response = () => axios.get('/v1/assets', {params: {gameAddr, assetId}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/assets`

This endpoint retrieves a specific asset from a specific game.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/assets`

### Arguments

Attribute    | Type      | Description                          | Required  | Max. length   |
------------ | --------- | ------------------------------------ | --------- |-------------- |
gameAddr     | string    | Address of the game contract.        | Yes       |       -       |
assetId      | number    | Id the asset.                        | Yes       |       -       |