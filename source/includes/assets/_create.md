# Assets

## Assets

- What is the Assets API used for?
- Prerequisites to use the Assets API.

## Create Asset

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Object containing the data needed to create an asset.
const assetData = {
    gameAddr,
    name: 'Asset Name',
    desc: 'Asset Description',
    image: 'Asset Image',
    rarity: 1,
    cap: 0,
    val: 100,
    timestamp: Date.now()
};

// Sign the data with your private key.
const assetDataJSON = stardust.createPostJSON.asset.add(assetData, privateKey);

// POST to the API endpoint '/v1/assets'.
// Send the assetDataJSON object in the request body.
// With the game address in the parameters.
const response = () => axios.post('/v1/assets', assetDataJSON, {params: {gameAddr}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/assets`

This endpoint creates a new asset.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/assets`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
name            | string    | Name of the asset.                                    | Yes       |       -       |
desc            | string    | Description of the asset.                             | Yes       |       -       |
image           | string    | Image of the asset.                                   | Yes       |       -       |
rarity          | number    | Rarity of the asset.                                  | Yes       |       -       |
cap             | number    | Maximum amount that can be minted. 0 for uncapped.    | Yes       |       -       |
val             | number    | Market value of the asset.                            | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |

Rarity Value    | Rarity Name       | Description                   |
--------------- | ----------------- | ----------------------------- |
1               | Common            | Common rarity type.           |
2               | Rare              | Rare rarity type.             |
3               | Super Rare        | Super rare rarity type.       |
4               | Limited Edition   | Limited edition rarity type.  |
5               | Unique            | Unique rarity type.           |