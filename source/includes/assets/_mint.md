## Mint Asset

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const assetId = 0;

// Object containing the data needed to mint an asset.
const mintData = {
    gameAddr,
    assetId,
    to: walletAddress,
    amount: 1,
    timestamp: Date.now()
};

// Sign the data with your private key.
const mintDataJSON = stardust.createPostJSON.asset.mint(mintData, privateKey);

// POST to the API endpoint '/v1/assets/mint'.
// Send the mintDataJSON object in the request body.
// With the game address and asset id in the parameters.
const response = () => axios.post('/v1/assets/mint', mintDataJSON, {params: {gameAddr, assetId}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/assets/mint`

This endpoint mints an asset.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/assets/mint`

### Arguments

Attribute    | Type      | Description                                          | Required  | Max. length   |
------------ | --------- | ---------------------------------------------------- | --------- | ------------- |
gameAddr     | string    | Address of the game contract.                        | Yes       |       -       |
assetId      | number    | Id the asset.                                        | Yes       |       -       |
to           | string    | Address of the asset receiver.                       | Yes       |       -       |
amount       | number    | Amount to be minted.                                 | Yes       |       -       |
timestamp    | number    | Current timestamp (UNIX). Used for authorization.    | Yes       |       -       |