# Boxes

## Boxes

- What is the Boxes API used for?
- Prerequisites to use the Boxes API.

## Create Box

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Object containing the data needed to add a box.
const boxData = {
    gameAddr,
    name: 'Box Name',
    desc: 'Box Description',
    image: 'Box Image',
    tokens: [0],
    timestamp: Date.now()
};

// Sign the data with your private key.
const boxDataJSON = stardust.createPostJSON.box.add(boxData, privateKey);

// POST to the API endpoint '/v1/boxes'.
// Send the boxDataJSON object in the request body.
// With the game address in the parameters.
const response = () => axios.post('/v1/boxes', boxDataJSON, {params: {gameAddr}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/boxes`

This endpoint creates a new box.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/boxes`

### Arguments

Attribute       | Type          | Description                                       | Required  | Max. length   |
--------------- | ------------- | ------------------------------------------------- | --------- | ------------- |
gameAddr        | string        | Address of the game contract.                     | Yes       |       -       |
name            | string        | Name of the box.                                  | Yes       |       -       |
desc            | string        | Description of the box.                           | Yes       |       -       |
image           | string        | Image of the box.                                 | Yes       |       -       |
tokens          | number array  | Array of asset id's the box will contain.         | Yes       |       -       |
timestamp       | number        | Current timestamp (UNIX). Used for authorization. | Yes       |       -       |