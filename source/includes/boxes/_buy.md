## Buy Box

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct box id.
const boxId = 0;

// Object containing the data needed to buy a box.
const buyData = {
    gameAddr,
    boxId,
    timestamp: Date.now()
};

// Sign the data with your private key.
const buyDataJSON = stardust.createPostJSON.box.buy(boxData, privateKey);

// POST to the API endpoint '/v1/boxes/buy'.
// Send the buyDataJSON object in the request body.
// With the game address and box id in the parameters.
const response = () => axios.post('/v1/boxes/buy', buyDataJSON, {params: {gameAddr, boxId}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/boxes/buy`

This endpoint buys a box.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/boxes/buy`

### Arguments

Attribute       | Type          | Description                                       | Required  | Max. length   |
--------------- | ------------- | ------------------------------------------------- | --------- | ------------- |
gameAddr        | string        | Address of the game contract.                     | Yes       |       -       |
boxId           | number        | Id of the box.                                    | Yes       |       -       |
timestamp       | number        | Current timestamp (UNIX). Used for authorization. | Yes       |       -       |