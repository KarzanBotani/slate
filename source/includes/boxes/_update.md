## Update Box

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct box id.
const boxId = 0;

// Object containing the data needed to update a box.
const updateData = {
    gameAddr,
    boxId,
    isValid: true,
    name: 'New Box Name',
    desc: 'New Box Description',
    image: 'New Box Image',
    tokens: [0],
    timestamp: Date.now()
};

// Sign the data with your private key.
const updateDataJSON = stardust.createPostJSON.box.update(boxData, privateKey);

// POST to the API endpoint '/v1/boxes/update'.
// Send the updateDataJSON object in the request body.
// With the game address and box id in the parameters.
const response = () => axios.post('/v1/boxes/update', updateDataJSON, {params: {gameAddr, boxId}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/boxes/update`

This endpoint updates a box.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/boxes/update`

### Arguments

Attribute       | Type          | Description                                       | Required  | Max. length   |
--------------- | ------------- | ------------------------------------------------- | --------- | ------------- |
gameAddr        | string        | Address of the game contract.                     | Yes       |       -       |
boxId           | number        | Id of the box.                                    | Yes       |       -       |
isValid         | boolean       | New status of the box.                            | Yes       |       -       |
name            | string        | New name of the box.                              | Yes       |       -       |
desc            | string        | New description of the box.                       | Yes       |       -       |
image           | string        | New image of the box.                             | Yes       |       -       |
tokens          | number array  | New array of asset id's the box will contain.     | Yes       |       -       |
timestamp       | number        | Current timestamp (UNIX). Used for authorization. | Yes       |       -       |