## Get Specific Loan

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const loanId = 0;

// GET a specific loan in a specific game from the API endpoint '/v1/loans'.
// With the game address and loan id in the parameters.
const response = () => axios.get('/v1/loans', {params: {gameAddr, loanId}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/loans`

This endpoint retrieves a specific asset from a specific game.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/loans`

### Arguments

Attribute    | Type      | Description                          | Required  | Max. length   |
------------ | --------- | ------------------------------------ | --------- |-------------- |
gameAddr     | string    | Address of the game contract.        | Yes       |       -       |
loanId       | number    | Id the loan.                         | Yes       |       -       |