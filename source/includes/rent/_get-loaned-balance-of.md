## Get Loaned Balance Of

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const assetId = 0;

// GET a user's loaned balance of a specific asset from the API endpoint '/v1/loans/loaned-balance-of'.
// With the game address, user address and asset id in the parameters.
const response = () => axios.get('/v1/loans/loaned-balance-of', {params: {gameAddr, userAddr: walletAddress, assetId}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/loans/loaned-balance-of`

This endpoint retrieves a user's loaned balance of a specific asset.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/loans/loaned-balance-of`

### Arguments

Attribute    | Type      | Description                          | Required  | Max. length   |
------------ | --------- | ------------------------------------ | --------- |-------------- |
gameAddr     | string    | Address of the game contract.        | Yes       |       -       |
userAddr     | string    | Address of the user.                 | Yes       |       -       |
assetId      | number    | Id the asset.                        | Yes       |       -       |