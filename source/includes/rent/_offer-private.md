# Rent

## Rent

- What is the Rent API used for?
- Prerequisites to use the Rent API.

## Offer Private Loan

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and destructure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const assetId = 0;

// Object containing the data needed to offer a private loan of an asset.
const offerData = {
    gameAddr,
    lender: walletAddress,
    borrower: '0xbA418a52A50c7169dbf7296D64B45a82DFa093Ce', // Exchange this for the correct user address.
    assetId,
    amount: 1,
    length: (20 * 24 * 60 * 60), // 20 days
    timestamp: Date.now()
}

// Sign the data with your private key.
const offerDataJSON = stardust.createPostJSON.loan.offerPrivate(offerData, privateKey);

// POST to the API endpoint '/v1/loans/offer-private'.
// Send the offerDataJSON object in the request body.
// With the game address and asset id in the parameters.
const response = () => axios.post('/v1/loans/offer-private', offerDataJSON, {params: {gameAddr, assetId}}).then(res => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/loans/offer-private`

This endpoint offers a private loan.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/loans/offer-private`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
lender          | string    | Address of the lender.                                | Yes       |       -       |
borrower        | string    | Address of the borrower.                              | Yes       |       -       |
assetId         | number    | Id the asset.                                         | Yes       |       -       |
amount          | number    | Amount to be offered.                                 | Yes       |       -       |
length          | number    | Length of the loan in seconds.                        | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |