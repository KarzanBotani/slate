## Handle Public Loan

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const loanId = 0;

// Object containing the data needed to offer a private loan of an asset.
const handleData = {
    gameAddr,
    loanId,
    timestamp: Date.now()
};

// Sign the data with your private key.
const handleDataJSON = stardust.createPostJSON.loan.handlePublic(handleData, privateKey);

// POST to the API endpoint '/v1/loans/handle-public'.
// Send the handleDataJSON object in the request body.
// With the game address and loan id in the parameters.
const response = () => axios.post('/v1/loans/handle-public', handleDataJSON, {params: {gameAddr, loanId}}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/loans/handle-public`

This endpoint handles a public loan.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/loans/handle-public`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
loanId          | number    | Id the loan.                                          | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |