## Get User Order

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct order id.
const orderId = 0;

// GET a specific order from a specific game from the API endpoint '/v1/shop/game-orders'.
// With the game address, user address and order id in the parameters.
const response = () => axios.get('/v1/shop/game-orders', {params: {gameAddr, userAddr: walletAddress, orderId}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`GET /v1/shop/game-orders`

This endpoint retrieves a user's specific order from a specific game.

### HTTP Request

`GET http://api.sandbox.stardust.com/v1/shop/orders`

### Arguments

Attribute    | Type      | Description                          | Required  | Max. length   |
------------ | --------- | ------------------------------------ | --------- |-------------- |
gameAddr     | string    | Address of the game contract.        | Yes       |       -       |
userAddr     | string    | Address of the user.                 | Yes       |       -       |
orderId      | number    | Id the order.                        | Yes       |       -       |