## Token To Token

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Object containing the data needed to create an order.
const orderData = {
    gameAddr,
    fromId: 0, // Exchange this for the correct asset id.
    fromAmount: 1,
    toId: 1, // Exchange this for the correct asset id.
    toAmount: 1,
    timestamp: Date.now()
}

// Sign the data with your private key.
const orderDataJSON = stardust.createPostJSON.shop.tokenToToken(orderData, privateKey);

// POST to the API endpoint '/v1/shop/token-to-token'.
// Send the orderDataJSON object in the request body.
// With the game address in the parameters.
const response = () => axios.post('/v1/shop/token-to-token', orderDataJSON, {params: {gameAddr}}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/shop/token-to-token`

This endpoint exchanges a token for another.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/shop/token-to-token`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
fromId          | number    | Id the asset you want to exchange from.               | Yes       |       -       |
fromAmount      | number    | Amount to be converted.                               | Yes       |       -       |
toId            | number    | Id the asset you want to exchange to.                 | Yes       |       -       |
fromAmount      | number    | Amount to be converted.                               | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |