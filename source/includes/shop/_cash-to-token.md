## Cash To Token

```javascript
const axios = require('axios'); // Import the axios package. We use it for calling the API.
const stardust = require('../libraries/star-dust'); // Import the Stardust library.
axios.defaults.baseURL = 'http://api.sandbox.stardust.com'; // Set a default base URL for axios.
axios.defaults.headers.post['Content-Type'] = 'application/json'; // Set the default header to JSON.

// Create a wallet and de-structure the address and private key.
const [walletAddress, privateKey] = stardust.createWallet();

// Exchange this for the correct game address.
const gameAddr = '0xa509a89479B08F734Bd4bD16A762eDcE7Ba44D95';

// Exchange this for the correct asset id.
const assetId = 0;

// Object containing the data needed to create an order.
const orderData = {
    gameAddr,
    assetId,
    amount: 1,
    timestamp: Date.now()
}

// Sign the data with your private key.
const orderDataJSON = stardust.createPostJSON.shop.tokenToCash(orderData, privateKey);

// POST to the API endpoint '/v1/shop/cash-to-token'.
// Send the orderDataJSON object in the request body.
// With the game address in the parameters.
const response = () => axios.post('/v1/shop/cash-to-token', orderDataJSON, {params: {gameAddr}}).then((res) => console.log(res.data));

response();
```

> Success Response:

```json
"success"
```

> Error Response:

```json
"error"
```

`POST /v1/shop/cash-to-token`

This endpoint converts cash to a token.

### HTTP Request

`POST http://api.sandbox.stardust.com/v1/shop/cash-to-token`

### Arguments

Attribute       | Type      | Description                                           | Required  | Max. length   |
--------------- | --------- | ----------------------------------------------------- | --------- | ------------- |
gameAddr        | string    | Address of the game contract.                         | Yes       |       -       |
assetId         | number    | Id the asset.                                         | Yes       |       -       |
amount          | number    | Amount to be converted.                               | Yes       |       -       |
timestamp       | number    | Current timestamp (UNIX). Used for authorization.     | Yes       |       -       |