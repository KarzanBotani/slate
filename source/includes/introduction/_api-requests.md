## API requests

To send a valid API request, combine the below components appropriately:

Component                               | Description
--------------------------------------- | ---------------------------------------
The HTTP method                         | <ul><li>`GET`. Requests data from server.</li><li>`POST`. Sends data to the server.</li></ul>
The URL to the API server               | <ul><li>Sandbox: `https://api.sandbox.stardust.com`</li><li>Live: `https://api.stardust.com`</li></ul>
The URL to the API resource             | The resource to interact with. For example: `v1/games`
A JSON request body                     | Required for most HTTP calls.

### HTTP Request Headers

The commonly used HTTP request headers are:

Headers                                 | Description
--------------------------------------- | ---------------------------------------
Accept                                  | Required for operations with a response body. Specifies the response format. <br/> The syntax is: `Accept: application/<format>`. <br/> When using the Stardust API, `<format>` is to be replaced with `json`.
Authorization                           | ?
Content-Type                            | Required for operations with a request body. Specifies the request format. <br/> The syntax is: `Content-Type: application/<format>`. <br/> When using the Stardust API, `<format>` is to be replaced with `json`.