# Introduction

The Stardust API is HTML-based. The request and response bodies are formatted in JSON.

<aside class="notice">You are not able to run the sample requests found in this guide as-is. Replace call-specific parameters with your own values.</aside>

## Getting Started

- Text on how to get started.