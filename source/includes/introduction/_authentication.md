## Authentication

> To authorize, use this code:

```javascript
bla
```

> Make sure to replace `xxx` with your API key.

Stardust uses API keys to allow access to the API. You can register a new Stardust API key at...

Stardust expects for the API key to be included in all API requests to the server in a header that looks like the following:

`Authorization: xxx`

<aside class="notice">You must replace <code>xxx</code> with your personal API key.</aside>