## API Responses

The Stardust API returns HTTP status codes and response bodies in JSON format containing information about the call or the resource the call was made to.

### HTTP success status codes

HTTP Status Code                                | Description
----------------------------------------------- | -----------------------------------------------
`200 OK`                                        | The request was successful.
`201 Created`                                   | A resource was successfully created.
`202 Accepted`                                  | The request was accepted.

### HTTP error status codes

HTTP Status Code                                | Description
----------------------------------------------- | -----------------------------------------------
`400 Bad Request`                               | Your request is invalid.
`401 Unauthorized`                              | Your API key is wrong.
`403 Forbidden`                                 | The resource requested is hidden for administrators only.
`404 Not Found`                                 | The specified resource could not be found.
`405 Method Not Allowed`                        | You tried to access a resource with an invalid method.
`406 Not Acceptable`                            | You requested a format that isn't JSON.
`410 Gone`                                      | The resource requested has been removed from our servers.
`429 Too Many Requests`                         | You're requesting too many resources! Slow down!
`500 Internal Server Error`                     | We had a problem with our server. Try again later.
`503 Service Unavailable`                       | We're temporarily offline for maintenance. Please try again later.

### Validation errors

A response with a status code of `400 Bad Request` will be returned if a request does not pass the validation check.

Parameter Type                                  | Description
----------------------------------------------- | -----------------------------------------------