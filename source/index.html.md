---
title: Stardust API Documentation

search: true

language_tabs:
  - node: Node

includes:
  - introduction/introduction
  # - introduction/authentication
  - introduction/api-requests
  - introduction/api-responses
  - assets/create
  - assets/trade
  - assets/mint
  - assets/get-all-assets
  - assets/get-specific-asset
  - assets/get-user-assets
  - boxes/create
  - boxes/buy
  - boxes/delete
  - boxes/get-all-boxes
  - boxes/get-specific-box
  - boxes/update
  - games/deploy
  - games/transfer
  - games/get-all-games
  - games/get-specific-game
  - games/get-balance-in-game
  - rent/offer-private
  - rent/offer-public
  - rent/handle-private
  - rent/handle-public
  - rent/finish
  - rent/get-specific-loan
  - rent/get-free-balance-of
  - rent/get-loaned-balance-of
  - rent/get-created-loans-count
  - rent/get-deleted-loans-count
  - shop/token-to-cash
  - shop/cash-to-token
  - shop/token-to-token
  - shop/get-specific-order
  - shop/get-order-count
  - shop/get-user-order
  - shop/get-all-user-orders
  - shop/get-user-order-count
  - trade/offer-private
  - trade/offer-public
  - trade/take-private
  - trade/take-public
  - trade/remove
  - trade/get-specific-trade
  - trade/get-user-trade
  - trade/get-all-user-trades
  - trade/get-user-trade-count
  - trade/get-user-trade-count-in-game
---